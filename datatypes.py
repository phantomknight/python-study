'I am string'
"I am string"

"""
Hello
I am Keaan
How are you?
"""

1 2 3 4 5 = integer
1.23 = float
2.45 = float

True - true
False - false

list = [1,2,3,4,5]
custom_list = [1, True, False, 2.24, 69]

dictionary = {
    "name": "Keaan",
    "age": 24
}
